from datetime import datetime

import websocket
import time
import json
import ssl
import csv

# Set up endpoint, you'll probably need to change this
cirrusAPIendpoint = "cirrus21.yanzi.se"

##########CHANGE BELOW TO YOUR OWN DATA##########

# Set up credentials. Please DONT have your credentials in your code when running on production
username = "soumya.panda@qbots.ai"
password = "somu*154259"

# Set up Location ID and Device ID, please change this to your own, can be found in Yanzi Live
locationID = "610974"  # Usually a 6 digit number
deviceID = "EUI64-0080E1030002BB05-3-Temp"
variableName = "temperatureC"
# deviceID = "EUI64-0080E1030002BB05-4-CO2"
# variableName = "carbonDioxide"

tempDataList = []
startTime = int(round(datetime(2019, 7, 5).timestamp() * 1000))
endTime = int(round(time.time() * 1000))
# endTime = int(round(datetime(2019, 7, 15).timestamp() * 1000))


################################################

# Handle message
def onMessage(ws, message):
    response = json.loads(message)
    if (response["messageType"] == "ServiceResponse") and (response["responseCode"]["name"] == "success"):
        print("ServiceRequest succeeded, sending LoginRequest")
        time.sleep(1)  # Note: Sleep is only used for presentation purposes, not actually needed in the integration.
        login(ws)
    elif (response["messageType"] == "LoginResponse") and (response["responseCode"]["name"] == "success"):
        print("LoginRequest succeeded, lets get some data...")
        time.sleep(1)  # Note: Sleep is only used for presentation purposes, not actually needed in the integration.
        getData(ws, locationID, deviceID, startTime, endTime)
    elif (response["messageType"] == "GetSamplesResponse") and (response["responseCode"]["name"] == "success"):
        if (response['sampleListDto']['range']['timeStart'] >= startTime) and ('list' in response["sampleListDto"]):
            tempDataList.append(response["sampleListDto"]["list"])
            getData(ws, locationID, deviceID,startTime, response['sampleListDto']['range']['timeStart'])
        else:
            ws.close()
    else:
        print("Error in: " + response["messageType"])
        print(response)
        ws.close()


# Handle error
def onError(ws, error):
    print("Error: " + error)


# Handle closing of websocket
def onClose(ws):
    with open('data.csv', mode='w') as data:
        writer = csv.writer(data, delimiter=',', lineterminator='\n', )
        writer.writerow(['SensorName', 'SensorDataPoint', 'Time', 'Value'])
        for data in tempDataList:
            for element in data:
                writer.writerow(["Comfort Test", deviceID,
                                 datetime.fromtimestamp(element['sampleTime'] / 1000.0).isoformat(),
                                 element['value']])
    print("Closing connection!")


# Handle opening of websocket
def onOpen(ws):
    print("Websocket open!")
    print("Checking API service status with ServiceRequest.")
    time.sleep(1)  # Note: Sleep is only used for presentation purposes, not actually needed in the integration.
    serviceRequest()


# Check API service status
def serviceRequest():
    ws.send(json.dumps({
        "messageType": "ServiceRequest"
    }))


# Login
def login(ws):
    ws.send(json.dumps({
        "messageType": "LoginRequest",
        "username": username,
        "password": password
    }))


# Get temperature for the last hour
def getData(ws, locationID, deviceID, startTimeMs, endTimeMs):
    print("Data received till " +  str(datetime.fromtimestamp(endTimeMs / 1000.0)))
    ws.send(json.dumps({
        "messageType": "GetSamplesRequest",
        "dataSourceAddress": {
            "resourceType": "DataSourceAddress",
            "did": deviceID,
            "locationId": locationID,
            "variableName": {
                "resourceType": "VariableName",
                "name": variableName
            }
        },
        "timeSerieSelection": {
            "resourceType": "TimeSerieSelection",
            "timeStart": startTimeMs,
            "timeEnd": endTimeMs
        },
        "timeSent": startTimeMs
    }))



if __name__ == "__main__":
    print("Connecting to " + cirrusAPIendpoint + " with user: " + username)
    time.sleep(1)  # Note: Sleep is only used for presentation purposes, not actually needed in the integration.

    ws = websocket.WebSocketApp("wss://" + cirrusAPIendpoint + "/cirrusAPI",
                                on_message=onMessage,
                                on_error=onError,
                                on_close=onClose)
    ws.on_open = onOpen
    ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
